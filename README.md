# [coconut](https://pypi.org/project/coconut/)

Simple, elegant, Pythonic functional programming.

* See also 'coconut-prelude'
* [*Introducing Coconut for making functional programming in Python simpler*
  ](https://hub.packtpub.com/introducing-coconut-for-making-functional-programming-in-python-simpler/)
  2019-01 Bhagyashree R
* [*Functional programming goes tropical with Coconut, a powerful Pythonic programming language*
  ](https://jaxenter.com/coconut-python-programming-language-154069.html)
  2019-01 Jane Elizabeth
* [*What do Python users think of Coconut?*
  ](https://www.quora.com/What-do-Python-users-think-of-Coconut)
  (2017) (Quora)
